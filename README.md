# Test script for S3-like endpoints

An S3 test script that will check the following things

 * S3 authentication (access key + secret key)
 * PUT
 * GET
 * GET with prefix matching
 * GET chunk
 * GET multiple chunks
 
## Installation

Clone the script from gitlab.cern.ch

This script uses the Davix utility

It's available in EPEL and Fedora

`# yum install davix`

More info on installing from source is available at

https://dmc-docs.web.cern.ch/dmc-docs/docs/davix-epel/html/

## Configuration

Put credentials in a file called `.creds` with the following 3 lines

```
endpoint="s3://bucket.service.domain.org"
secret="..."
access="..."
```

To use https, use the "s3s" protocol designation
```
endpoint="s3s://bucket.service.domain.org"
```

If you're using an S3 endpoint with the form
`s3://region.amazonaws.com/bucket`
then edit the script to uncomment the following
```
davix_opts="${davix_opts} --s3alternate"
```

To trigger AWS v4 authentication, specify the region.
Edit the script to uncomment the following
```
davix_opts="${davix_opts} --s3region eu-west-1"`
```

## Execution

`$ ./s3test.sh`

Check `test.log` file for detailed test output
