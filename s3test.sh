#/bin/bash

# Test script for S3-like endpoints
 
# Please see accompanying README for details on tests, installation
# and configuration

# --------------------------------------------------------------
# User Configuration

# Put credentials in file called .creds with the following 3 lines
# endpoint="s3://bucket.service.domain.org"
# secret="..."
# access="..."

# If you're using an S3 endpoint with the form
# endpoint="s3://region.amazonaws.com/bucket"
# then use this option
davix_opts="${davix_opts} --s3alternate" 

# For an https endpoint, use the s3s protocol
# endpoint="s3s://bucket.service.domain.org"

# To trigger AWS v4 authentication, specify the region
#davix_opts="${davix_opts} --s3region eu-west-1"

# End of user configuration
# --------------------------------------------------------------

# Load config

if [ ! -f .creds ]; then
	echo "Can't find credintials in .creds"
	echo "Quitting"
	exit 1
else
	. .creds
fi

# Some setup

fail="\e[31mFailure\e[39;49m"
succeed="\e[32mSuccess\e[39;49m"
timeout=10 # seconds
log="test.log"

# Some functions

cleanup () {
	echo
	echo "Cleaning test files from endpoint"
	for obj in tf01 td01/tf01 td01/; do
		if ( davix-ls ${davix_opts} -l --s3secretkey ${secret} --s3accesskey ${access} ${endpoint}/${obj} &> /dev/null ); then
			davix-rm ${davix_opts} --s3secretkey ${secret} --s3accesskey ${access} ${endpoint}/${obj}
		fi
	done
	echo
}

test_start () {
	echo $testinfo
	exec 3>&1
	exec 4>&2
	exec 1> ${tdir}/stdout
	exec 2> ${tdir}/stderr
	echo "-----------------------------------"
	echo $testinfo
	set -xv
}

test_end() {
	set +xv
	exec 1>&3
	exec 2>&4
	cat ${tdir}/stdout >> ${log}
	echo "STDERR" >> ${log}
	sed -e "s/${secret}/\${secret}/g" ${tdir}/stderr >> ${log}
	echo >> ${log}
	if [ $1 -ne 0 ]; then
		echo -e $fail
		echo "Failure" >> ${log}
		return 1
	else
		echo -e $succeed
		echo "Success" >> ${log}
		return 0
	fi
}

if [ -f ${log} ]; then
	rm ${log}
fi

# Is davix installed?
if ! which davix-ls &> /dev/null; then
	echo "No davix, quitting"
	exit 1
fi

# Create a temp directory for uploads and downloads
tdir=`mktemp -d`

echo "Starting to test ${endpoint}"
echo

# Do we have access?
testinfo="Checking Access"
test_start
timeout ${timeout} davix-ls ${davix_opts} -l --s3secretkey ${secret} --s3accesskey ${access} ${endpoint}
test_end $?
if [ $? -ne 0 ]; then
	echo "No access, quitting"
	rm -rf ${tdir}
	exit 1
fi

# Create a test file
#size=2048 # blocks
size=16 # blocks
dd if=/dev/urandom of=${tdir}/1M count=${size} 2> /dev/null
echo MARK01 | dd seek=4 of=${tdir}/1M conv=notrunc 2> /dev/null
echo MARK02 | dd seek=8 of=${tdir}/1M conv=notrunc 2> /dev/null

# Clean up the endpoint
cleanup

# Start testing

# Can we put?
testinfo="Trying PUT"
test_start
timeout ${timeout} davix-put ${davix_opts} --s3secretkey ${secret} --s3accesskey ${access} ${tdir}/1M ${endpoint}/tf01 
test_end $?
if [ $? -ne 0 ]; then
	echo "Can't put, quitting"
	rm -rf ${tdir}
	exit 1
fi
testinfo="Checking PUT"
test_start
timeout ${timeout} davix-ls ${davix_opts} -l --s3secretkey ${secret} --s3accesskey ${access} ${endpoint}/tf01
test_end $?
echo

# Can we get?
testinfo="Trying GET"
test_start
timeout ${timeout} davix-get ${davix_opts} --s3secretkey ${secret} --s3accesskey ${access} ${endpoint}/tf01 ${tdir}/1M.get
test_end $?
testinfo="Checking GET"
test_start
diff ${tdir}/1M ${tdir}/1M.get
test_end $?
echo

# Can we get a byte range?
testinfo="Trying Range request 1"
test_start
timeout ${timeout} davix-get ${davix_opts} -H "Range: bytes=2048-2053" --s3secretkey ${secret} --s3accesskey ${access} ${endpoint}/tf01 ${tdir}/1M.get.r1
test_end $?
testinfo="Checking Range request"
test_start
cat ${tdir}/1M.get.r1 && echo
test `cat ${tdir}/1M.get.r1` == "MARK01"
test_end $?

testinfo="Trying Range request 2"
test_start
timeout ${timeout} davix-get ${davix_opts} -H "Range: bytes=4096-4101" --s3secretkey ${secret} --s3accesskey ${access} ${endpoint}/tf01 ${tdir}/1M.get.r2
test_end $?
testinfo="Checking Range request"
test_start
cat ${tdir}/1M.get.r2 && echo
test `cat ${tdir}/1M.get.r2` == "MARK02"
test_end $?
echo

# Can we get a double byte range?
testinfo="Trying double range request"
test_start
timeout ${timeout} davix-get ${davix_opts} -H "Range: bytes=2048-2054,4096-4101" --s3secretkey ${secret} --s3accesskey ${access} ${endpoint}/tf01 ${tdir}/1M.get.rr
test_end $?
testinfo="Checking first chunk"
test_start
# Check mark is there, and we didn't just get the whole file
grep -q MARK01 ${tdir}/1M.get.rr && ! diff ${tdir}/1M ${tdir}/1M.get.rr
test_end $?
testinfo="Checking second chunk"
test_start
# Check mark is there, and we didn't just get the whole file
grep -q MARK02 ${tdir}/1M.get.rr && ! diff ${tdir}/1M ${tdir}/1M.get.rr
test_end $?
echo

# Can we delete?
testinfo="Trying Delete"
test_start
# Broken in davix 0.6.6 as davix-rm always returns 0
timeout ${timeout} davix-rm ${davix_opts} --s3secretkey ${secret} --s3accesskey ${access} ${endpoint}/tf01 ${tdir}/1M.get
test_end $?
echo

# Can we create a dir?
testinfo="Trying Directory Creation"
test_start
timeout ${timeout} davix-put ${davix_opts} --s3secretkey ${secret} --s3accesskey ${access} ${tdir}/1M ${endpoint}/td01/tf01
test_end $?
testinfo="Checking Directory Creation"
test_start
out=`davix-ls ${davix_opts} --s3secretkey ${secret} --s3accesskey ${access} ${endpoint}/td01 | awk '{print $NF}'`
[ x${out} = "xtf01" ]
test_end $?

# Clean up
#rm -rf ${tdir}
cleanup
